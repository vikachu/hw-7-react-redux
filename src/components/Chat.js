import React from "react";
import { connect } from "react-redux";
import moment from "moment";
import * as actions from "../actions/messageActions";
import { setEditingMessage, showModal } from "../actions/modalActions";
import "../styles/Chat.scss";

import Header from "./Header";
import MessageList from "./MessageList";
import MessageInput from "./MessageInput";
import LoadingSpinner from "./LoadingSpinner";
import Logo from "./Logo";
import Copyright from "./Copyright";
import EditMessageForm from "./EditMessageForm";

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      editingMessageId: "",
      editInput: "",
      input: "",
    };
    this.onMessageInputChange = this.onMessageInputChange.bind(this);
    this.onLikeClick = this.onLikeClick.bind(this);
    this.onDeleteMessageClick = this.onDeleteMessageClick.bind(this);
    this.onEditMessageClick = this.onEditMessageClick.bind(this);

    this.onAddAllMessages = this.onAddAllMessages.bind(this);
    this.onSendMessageClick = this.onSendMessageClick.bind(this);
  }

  componentDidMount() {
    fetch("https://edikdolynskyi.github.io/react_sources/messages.json")
      .then((res) => res.json())
      .then((res) => {
        return res.sort((a, b) => moment(a.createdAt).diff(b.createdAt));
      })
      .then(
        (result) => {
          const randomUser = result[0];
          const currentUser = {
            id: randomUser.userId,
            name: randomUser.user,
            avatar: randomUser.avatar,
          };

          this.onAddAllMessages(result, currentUser);

          this.setState({
            isLoaded: true,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );
  }

  onAddAllMessages(messages, currentUser) {
    this.props.addAllMessages(messages, currentUser);
  }

  async onMessageInputChange(e) {
    const value = e.target.value;
    await this.setState({
      ...this.state,
      input: value,
    });
  }

  onSendMessageClick() {
    if (this.state.input !== "") {
      const message = {
        text: this.state.input,
      };
      this.props.sendMessage(message);

      this.setState({
        ...this.state,
        input: "",
      });
    }
  }

  onLikeClick(clickedMessage) {
    if (this.props.currentUser.id !== clickedMessage.userId) {
      this.props.likeMessage(clickedMessage.id);
    }
  }

  onDeleteMessageClick(clickedMessage) {
    if (this.props.currentUser.id === clickedMessage.userId) {
      this.props.deleteMessage(clickedMessage.id);
    }
  }

  onEditMessageClick(clickedMessage) {
    if (this.props.currentUser.id === clickedMessage.userId) {
      this.props.setEditingMessage(clickedMessage);
      this.props.showModal();
    }
  }

  render() {
    if (!this.state.isLoaded) {
      return <LoadingSpinner />;
    } else {
      const messages = this.props.messages;
      const userIds = new Set();
      messages.map((message) => userIds.add(message.userId));

      const lastMessageTime = moment(
        messages[messages.length - 1].createdAt
      ).format("hh:mm");

      return (
        <div>
          <div className="chat__container">
            <Logo />
            <Header
              messagesCount={messages.length}
              participantsCount={userIds.size}
              lastMessageTime={lastMessageTime}
            />
            <MessageList
              messages={this.props.messages}
              currentUserId={this.props.currentUser.id}
              onLikeClick={this.onLikeClick}
              onDeleteMessageClick={this.onDeleteMessageClick}
              onEditMessageClick={this.onEditMessageClick}
            />
            <MessageInput
              onMessageInputChange={this.onMessageInputChange}
              onSendMessageClick={this.onSendMessageClick}
              inputValue={this.state.input}
            />
            <Copyright />
          </div>
          <EditMessageForm />
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.messagesReducer.messages,
    currentUser: state.messagesReducer.currentUser,
  };
};

const mapDispatchToProps = {
  ...actions,
  setEditingMessage,
  showModal,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
