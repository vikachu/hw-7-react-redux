import React from "react";
import PropTypes from "prop-types";
import "../styles/Header.scss";

class Header extends React.Component {
  render() {
    const props = this.props;

    return (
      <div className="header__container">
        <div className="header__info-wrapper">
          <div className="header__name">My Chat</div>

          <div className="header__participants">
            <span>{props.participantsCount}</span>
            <span>participants</span>
          </div>

          <div className="header__messages">
            <span>{props.messagesCount}</span>
            <span>messages</span>
          </div>
        </div>

        <div className="header__last-message">
          <span>last message at</span>
          <span>{props.lastMessageTime}</span>
        </div>
      </div>
    );
  }
}

Header.propTypes = {
  messagesCount: PropTypes.number,
  participantsCount: PropTypes.number,
  lastMessageTime: PropTypes.string,
};

export default Header;
