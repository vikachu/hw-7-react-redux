import React from "react";
import { connect } from "react-redux";
import "../styles/EditMessageForm.scss";
import * as actions from "../actions/modalActions";
import { editMessage } from "../actions/messageActions";

class EditMessageForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editMessageInput: "",
    };
  }

  componentWillMount() {
    this.onEditMessageInputChange = this.onEditMessageInputChange.bind(this);
    this.onCancelButtonClick = this.onCancelButtonClick.bind(this);
    this.onSaveButtonClick = this.onSaveButtonClick.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ...this.state,
      editMessageInput: nextProps.editingMessage.text,
    });
  }

  async onEditMessageInputChange(e) {
    const value = e.target.value;
    await this.setState({
      ...this.state,
      editMessageInput: value,
    });
  }

  onCancelButtonClick() {
    this.props.hideModal();

    this.setState({
      ...this.state,
      editMessageInput: "",
    });
  }

  onSaveButtonClick() {
    this.props.editMessage(this.props.editingMessage.id, {
      text: this.state.editMessageInput,
    });

    this.props.hideModal();
    this.setState({
      ...this.state,
      editMessageInput: "",
    });
  }

  getEditMessageForm() {
    return (
      <div className="modal__layer">
        <div className="modal__root">
          <div className="modal__header">Edit message</div>
          <div className="modal__body">
            <textarea
              className="modal_text"
              value={this.state.editMessageInput}
              onChange={this.onEditMessageInputChange}
            ></textarea>
          </div>
          <div className="modal__footer">
            <button
              className="modal_button modal_button-cancel"
              onClick={this.onCancelButtonClick}
            >
              Cancel
            </button>

            <button
              className="modal_button modal_button-save"
              onClick={this.onSaveButtonClick}
            >
              Save
            </button>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const isShown = this.props.isShown;
    return isShown ? this.getEditMessageForm() : null;
  }
}

const mapStateToProps = (state) => {
  return {
    editingMessage: state.modalReducer.editingMessage,
    isShown: state.modalReducer.isShown,
  };
};

const mapDispatchToProps = {
  ...actions,
  editMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditMessageForm);
