import React from "react";
import PropTypes from "prop-types";
import "../styles/SeparatorLine.scss";

class SeparatorLine extends React.Component {
  render() {
    return (
      <div className="separator-line__container">
        <div className="separator-line__line">
          <div className="separator-line__text">{this.props.separatorDate}</div>
        </div>
      </div>
    );
  }
}

SeparatorLine.propTypes = {
  separatorDate: PropTypes.string,
};

export default SeparatorLine;
