import React from "react";
import "../styles/LoadingSpinner.scss";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class LoadingSpinner extends React.Component {
  render() {
    return (
      <div className="loading-spinner__container">
        <FontAwesomeIcon icon={faSpinner} />
      </div>
    );
  }
}

export default LoadingSpinner;
