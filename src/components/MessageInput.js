import React from "react";
import PropTypes from "prop-types";
import "../styles/MessageInput.scss";

class MessageInput extends React.Component {
  render() {
    return (
      <div className="message-input__container">
        <textarea
          className="message-input__text"
          placeholder="Message"
          onChange={this.props.onMessageInputChange}
          value={this.props.inputValue}
        ></textarea>

        <button
          className="message-input__button"
          onClick={this.props.onSendMessageClick}
        >
          Send
        </button>
      </div>
    );
  }
}

MessageInput.propTypes = {
  onMessageInputChange: PropTypes.func.isRequired,
  onSendMessageClick: PropTypes.func.isRequired,
  inputValue: PropTypes.string,
};

export default MessageInput;
