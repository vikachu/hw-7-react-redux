import React from "react";
import "../styles/Copyright.scss";

class Copyright extends React.Component {
  render() {
    return (
      <div className="copyright__container">
          Copyright
      </div>
    );
  }
}

export default Copyright;
