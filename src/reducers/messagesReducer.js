import moment from "moment";
import { actionTypes } from "../actions/messageActionTypes";

const initialState = {
  messages: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.SEND_MESSAGE: {
      const { data } = action.payload;
      const newMessage = {
        ...data,
        id: Math.random().toString().substr(2, 8),
        createdAt: moment(),
        editedAt: "",
        likes: 0,
        user: state.currentUser.name,
        avatar: state.currentUser.avatar,
        userId: state.currentUser.id,
      };
      return { ...state, messages: [...state.messages, newMessage] };
    }

    case actionTypes.LIKE_MESSAGE: {
      const { id } = action.payload;
      const updatedMessages = state.messages.map((message) => {
        return message.id === id
          ? {
              ...message,
              likes: message.likes + 1,
            }
          : message;
      });
      return {
        ...state,
        messages: updatedMessages,
      };
    }

    case actionTypes.EDIT_MESSAGE: {
      const { id, data } = action.payload;
      const editedMessages = state.messages.map((message) => {
        return message.id === id
          ? {
              ...message,
              ...data,
              editedAt: moment(),
            }
          : message;
      });
      return {
        ...state,
        messages: editedMessages,
      };
    }

    case actionTypes.DELETE_MESSAGE: {
      const { id } = action.payload;
      const filteredMessages = state.messages.filter((message) => message.id !== id);
      return {
        ...state,
        messages: filteredMessages,
      };
    }

    case actionTypes.ADD_ALL_MESSAGES: {
      const { messages, currentUser } = action.payload;
      const updatedMessages = messages.map((message) => ({
        ...message,
        likes: 0,
      }));
      return {
        ...state,
        messages: updatedMessages,
        currentUser: currentUser,
      };
    }

    default:
      return state;
  }
}
