import { combineReducers } from "redux";
import messagesReducer from "./messagesReducer";
import modalReducer from "./modalReducer";

const rootReducer = combineReducers({
  messagesReducer,
  modalReducer,
});

export default rootReducer;
