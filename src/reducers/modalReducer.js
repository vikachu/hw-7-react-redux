import { actionTypes } from "../actions/modalActionTypes";

const initialState = {
  editingMessage: {
    id: "",
    text: "",
  },
  isShown: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.SHOW_MODAL: {
      return {
        ...state,
        isShown: true,
      };
    }

    case actionTypes.HIDE_MODAL: {
      return {
        ...state,
        isShown: false,
      };
    }

    case actionTypes.SET_EDITING_MESSAGE: {
      const { message } = action.payload;
      return {
        ...state,
        editingMessage: {
          id: message.id,
          text: message.text,
        },
        isShown: false,
      };
    }

    default:
      return state;
  }
}
