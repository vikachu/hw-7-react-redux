import { actionTypes } from "./messageActionTypes";

export const sendMessage = (data) => ({
  type: actionTypes.SEND_MESSAGE,
  payload: {
    data,
  },
});

export const likeMessage = (id) => ({
  type: actionTypes.LIKE_MESSAGE,
  payload: {
    id: id,
  },
});

export const editMessage = (id, data) => ({
  type: actionTypes.EDIT_MESSAGE,
  payload: {
    id: id,
    data,
  },
});

export const deleteMessage = (id) => ({
  type: actionTypes.DELETE_MESSAGE,
  payload: {
    id,
  },
});

export const addAllMessages = (messages, currentUser) => ({
  type: actionTypes.ADD_ALL_MESSAGES,
  payload: {
    messages,
    currentUser
  }
});
