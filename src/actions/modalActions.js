import { actionTypes } from "./modalActionTypes";

export const showModal = () => ({
  type: actionTypes.SHOW_MODAL,
});

export const hideModal = () => ({
  type: actionTypes.HIDE_MODAL,
});

export const setEditingMessage = (message) => ({
  type: actionTypes.SET_EDITING_MESSAGE,
  payload: {
    message,
  }
});
